import { Injectable } from '@angular/core';
import { KeycloakInstance } from 'keycloak-js';

declare var Keycloak:any;
@Injectable({
  providedIn: 'root'
})
export class KeycloakSecurityService {
  public kc: KeycloakInstance = new Keycloak;

  constructor() { }
  async init(){
    console.log("security initialisation");
    this.kc=new Keycloak(
      {
        url:"http://localhost:8080/",
        realm:"master",
        clientId:"AngularProductsApp"
      } );
      await this.kc.init({
        onLoad:"check-sso",
      //  promiseType:'native' as KeycloakPromiseType
            });
    console.log(this.kc.token);
  }
}
